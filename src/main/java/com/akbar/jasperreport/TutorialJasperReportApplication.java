package com.akbar.jasperreport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TutorialJasperReportApplication {

	public static void main(String[] args) {
		SpringApplication.run(TutorialJasperReportApplication.class, args);
	}

}
