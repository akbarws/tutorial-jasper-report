package com.akbar.jasperreport.request;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.NonNull;

@JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductRequest {

	@NonNull
	private String name;
	@NonNull
	private Integer price;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	public ProductRequest() { }
	
	public ProductRequest(@NonNull String name, @NonNull Integer price) {
		super();
		this.name = name;
		this.price = price;
	}
	
	
}
