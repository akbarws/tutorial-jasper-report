package com.akbar.jasperreport.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.akbar.jasperreport.request.ProductRequest;
import com.akbar.jasperreport.service.ProductService;

@RestController
public class ProductController {

	@Autowired
	ProductService userService;
	
	@PostMapping(value = "/product/add", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> add(@RequestBody ProductRequest userRequest) {
		return ResponseEntity.ok(userService.add(userRequest));
	}
	
	@GetMapping(value = "/products", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> get() {
		return ResponseEntity.ok(userService.get());
	}
	
	@GetMapping(value = "/products/export")
	public ResponseEntity<?> report(HttpServletResponse httpServletResponse) throws IOException {
		httpServletResponse.setContentType("application/pdf");
        httpServletResponse.setHeader("Content-disposition", "attachment;filename=VoucherReport.pdf");
		return ResponseEntity.ok(userService.report(httpServletResponse.getOutputStream()));
	}
	
}
